## GOOGLE MAPS JAVASCRIPT API v3

## Reference to the <system-data-structure> node
#set ( $data = $_XPathTool.selectSingleNode($contentRoot, "/system-index-block/calling-page/system-page/system-data-structure") )

## HTML <id>
#set ( $id = $_SerializerTool.serialize($data.getChild("id"), true) )

## Block <title>
#set ( $map_title = $_SerializerTool.serialize($data.getChild("title"), true) )
 
## Map size (<width> and <height>) in pixels
#set ( $width  = $_SerializerTool.serialize($data.getChild("width"), true) )
#set ( $height = $_SerializerTool.serialize($data.getChild("height"), true) )
#set ( $dimType = $_SerializerTool.serialize($data.getChild("dimType"), true) )
#set ( $dim = ""  )

## Map <center>, equidistant from all edges
#set ( $center = $_SerializerTool.serialize($data.getChild("center"), true) )
 
## Map <zoom> level to start with
#set ( $zoom = $data.getChild("zoom").value )
 
## Which map <type> should be used
#set ( $type = $data.getChild("type").value )
 
## Set KML Layer
#set ( $kml = $data.getChild("overlays").getChild("kml").getChild("path").value )
 
## Reference to the <marker> nodes
#set ( $markers = $_XPathTool.selectNodes($data, "marker[use='Yes']") )

##Get category block to get Icons and Categories
#set ( $categoryBlockPath = $data.getChild("overlays").getChild("categoryBlock").getChild("path").value )
#set ( $categoryBlock = $_.locateBlock($categoryBlockPath) )

## Site URL for prepending to map icons and kml layer
#set ( $siteURL = $_EscapeTool.xml($categoryBlock.getStructuredDataNode("siteURL").textValue) )
 
## Check that the list of markers is not empty
<div class="container-map">

    #if ( $markers.size() > 0 )
     
        ## Output the HTML for the interactive embed
    
        <script type="text/javascript">
        
            $(document).ready(function() {  initialize(); });
            
            var markers = []; 
             
            var map_options = {
                center: new google.maps.LatLng(${center}), 
                zoom: ${zoom},
                mapTypeId: google.maps.MapTypeId.${type},
                streetViewControl: false,
                scrollwheel: true,
                draggable: true
            };
                
            var ctaLayer;            
            var google_map; 
            var hhMapId = "${id}";
                
            function initialize() {
                
                google_map = new google.maps.Map(document.getElementById("${id}"), map_options);
                
                ctaLayer = new google.maps.KmlLayer({
                    url: '${siteURL}${kml}',
                    preserveViewport: true,
                    clickable: false
                });
                            
                ## Initialize the InfoWindow
                var info_window = new google.maps.InfoWindow({
                    content: 'loading'
                });
                
                ctaLayer.setMap(google_map);
                
                ## Set Marker Icons
                #set ( $categories = $categoryBlock.getStructuredDataNodes("color") )
                #foreach ( $category in $categories )
                    #set ( $iconPath = $category.getChild("icon").asset.path )
                    #set ( $varCatName = $category.getChild("category").textValue.replaceAll(" ","") )
                    var $varCatName = '${siteURL}/${iconPath}';
                #end
    
                #set ( $i = 1 )
                #foreach ( $marker in $markers )
                    ## The <title> of the marker
                    #set ( $mTitle = $marker.getChild("title") )
                    #set ( $mTitle = $_SerializerTool.serialize($mTitle, true) )
                    #set ( $mTitle = $mTitle.replaceAll("'","&#92;&#39;") )
                                    
                    ## The category of marker
                    #set ( $cat = $marker.getChild("category").value.replaceAll(" ","") )
                    
                    ## The <coords> of the marker
                    #set ( $coords = $_SerializerTool.serialize($marker.getChild("coords"), true) )
                    
                    ## Content for the <info> window
                    #set ( $infoWindow = $marker.getChild("infoWindow") )
    
                    ##Get image
                    #if ($infoWindow.getChild("img").getChild("path").value != "/")
                        #set ( $link = $infoWindow.getChild("img").getChild("link").value )
                        #set ( $img = '<div style="float:right"><img src="' + ${link} + '" style="margin:0px; padding:5px;" /></div>' )
                    #else
                        #set ( $img = '' )
                    #end
                    
                    ##Get main details for info window
                        #set( $infoParagraphs = $_XPathTool.selectNodes($infoWindow, "infoWindowContent[. != '']") )
                        #set( $basicInfo = '' )
                        #foreach( $infoParagraph in $infoParagraphs )
                            #set( $basicInfo = $basicInfo + '<p>' + $_EscapeTool.javascript($infoParagraph.value) + '</p>' )
                        #end
    
                    
                    ##Get tabs groupings
                    #set ( $tabs = $_XPathTool.selectNodes($infoWindow, "tabs") )
                    
                    ## Tab Content
                    #set ( $tabsHtml = "" )
                    
                    #if ( $infoWindow.getChild("infoWindowStyle").getChild("value").value == "Add Tabs to Information Window" )
                        #if ( $img != '' )
                            #set ( $tabIncrement = 1)
                            #set ( $checkedAttr = "checked=${_EscapeTool.q}checked${_EscapeTool.q}" )
                            #set ( $tabsHtml = "${tabsHtml} <li><input type=${_EscapeTool.q}radio${_EscapeTool.q} name=${_EscapeTool.q}tabs${_EscapeTool.q} id=${_EscapeTool.q}tab${tabIncrement}${_EscapeTool.q} ${checkedAttr} /><label for=${_EscapeTool.q}tab${tabIncrement}${_EscapeTool.q}>Details</label><div id=${_EscapeTool.q}tab-content${tabIncrement}${_EscapeTool.q} class=${_EscapeTool.q}tab-content${_EscapeTool.q}>" )    
                            #set ( $tabsHtml = "${tabsHtml} ${img} ${basicInfo}" )    
                            #set ( $tabsHtml = "${tabsHtml} </div></li>")
                        #else
                            #set ( $tabIncrement = 0)
                        #end
            
                        #foreach ( $tab in $tabs )
                            #set ( $tabIncrement = $tabIncrement + 1 )
                            #set ( $title = $tab.getChild("title").value )
                            #set ( $type = $tab.getChild("type").value )
                            #set ( $content = $tab.getChild("content") )
                            #set ( $checkedAttr = "" )
                            #if ( $tabIncrement == 1 )
                                #set ( $checkedAttr = "checked=${_EscapeTool.q}checked${_EscapeTool.q}" )
                            #end
                            
                            ##Opening of tab content
                            #set ( $tabsHtml = "${tabsHtml} <li><input type=${_EscapeTool.q}radio${_EscapeTool.q} name=${_EscapeTool.q}tabs${_EscapeTool.q} id=${_EscapeTool.q}tab${tabIncrement}${_EscapeTool.q} ${checkedAttr} /><label for=${_EscapeTool.q}tab${tabIncrement}${_EscapeTool.q}>${title}</label><div id=${_EscapeTool.q}tab-content${tabIncrement}${_EscapeTool.q} class=${_EscapeTool.q}tab-content${_EscapeTool.q}>" )    
        
                            #if ( $type == "Text" )
                                #set ( $tabsHtml = "${tabsHtml} ${_EscapeTool.javascript($content.value)}" )    
                            #elseif ( $type == "Links")
                                #set ( $linkLi = "" )
                                #set ( $links = $_XPathTool.selectNodes($tab, "link") )
                                #foreach ( $link in $links )
                                    #set ( $url = $link.getChild("link").value )
                                    #set ( $linkTitle = $link.getChild("title").value )
                                    #set ( $linkLi = ${linkLi} + '<li style="display: list-item !important; float: none !important; list-style-type: disc;"><a href="' + ${url}+ '" target="_blank">' + ${linkTitle}+ '</a></li>')
                                    ## foreach code
                                #end
                                #set ( $margin = 'style="margin-left:20px; padding-top: 10px;"' )
                                #set ( $tabsHtml = "${tabsHtml} ${_EscapeTool.javascript($content.value)}<br /><ul ${margin}>${linkLi}</ul>" )
                            #elseif ( $type == "Video" )
                                #set ( $youtubeID = $tab.getChild("youtubeID").value )
                                #set ( $videoHtml = '<div style="float:right;"><embed width="200" height="145" src="//www.youtube.com/embed/' + ${youtubeID} + '?autohide=1allowfullscreen=1" /></div>' )
                                #set ( $tabsHtml = "${tabsHtml} ${videoHtml}<p>${_EscapeTool.javascript($content.value)}</p>" )
                            #elseif ( $type == "Pictures" )
                                #set ( $photoHtml = "" )
                                #set ( $photos = $_XPathTool.selectNodes($tab, "photo") )
                                #foreach ( $photo in $photos )
                                    #set ( $photoPath = $photo.getChild("link").value )
                                    #set ( $photoHtml = ${photoHtml} + '<img src="' + ${photoPath} + '" class="thumbnail" width="400px" />')
                                #end
                                #set ( $tabsHtml = "${tabsHtml} ${photoHtml}" )
                            #end
                            
                            ##Closing of tab content
                            #set ( $tabsHtml = "${tabsHtml} </div></li>")
                        #end
                    
                        ## All tabbed content now in here
                        #set ( $info = '<div class="infowindowTabs" syle="height:150px"><h2>' + ${mTitle} + '</h2><a href="https://www.google.com/maps/dir/Current+Location/' + ${coords} + '" target="_blank">Get Directions</a><ul class="tabs">' + ${tabsHtml} + '</ul></div>' ) 
                        #set ( $info = $info.replaceAll("&lt;","<") )
                        #set ( $info = $info.replaceAll("&gt;",">") )
                        #set ( $info = $info.replaceAll("'","&#92;&#39;") )
                    
                    #else
                        #set ( $info = '<div class="infowindow">' + ${img} + '<h2>' + ${mTitle} + '</h2><a href="https://www.google.com/maps/dir/Current+Location/' + ${coords} + '" target="_blank">Get Directions</a>' + ${basicInfo} + '</div>' )
                        #set ( $info = $info.replaceAll("&lt;","<") )
                        #set ( $info = $info.replaceAll("&gt;",">") )
                        #set ( $info = $info.replaceAll("'","&#92;&#39;") )
                    #end
                    var m${i} = new google.maps.Marker({
                        map:       google_map,
                        animation: google.maps.Animation.DROP,
                        title:     '${mTitle}',
                        icon:      ${cat},
                        position:  new google.maps.LatLng($coords),
                        html:      '${info}'
                    });
                    
                    google.maps.event.addListener(m${i}, 'click', function() {
                        info_window.setContent(this.html);
                        info_window.open(google_map, this);
                    });
     
                   markers.push(m${i});
                   
                    ## Increment the $i counter
                    #set ( $i = $i + 1 )
                #end
            }
        </script>  
    
        #if ( $dimType == "Pixel" )
            #set ( $dim = "px" )
        #else
            #set ( $dim = "%" )
        #end
        
        <div id="${id}" style="margin:10px;height:${height}${dim};width:${width}${dim}">${map_title}</div>
     
    #end
</div>

## Format URL: http://cascade.umt.edu/entity/open.act?id=956009b60a0a07cc60c45c274e92acd3&type=format&#highlight

## Audits
