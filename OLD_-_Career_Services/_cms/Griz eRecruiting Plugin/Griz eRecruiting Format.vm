#set ( $root = $_XPathTool.selectSingleNode($contentRoot, ".") )
#set ( $output =  "#CSO($root)" )
$output.trim()

#*
    CSO
    
    Main function for outputting the CSO plugin's markup
    
    @param $root the system page structure/contentRoot

*#
#macro ( CSO $root )
    ## Get the data from the Data Definition
    #set ( $majors = $_XPathTool.selectNodes($root, "job_Major_list/majorvalue") )
    #set ( $validMajorList =  "#NodesToCommaSeperatedString($majors)" )
    
    ## Advanced Options
    #set ($advanced_options= $_XPathTool.selectSingleNode($root, "advanced"))
    
    #if ($advanced_options.getChild("showDocs").value != "")
        #set ($showDocs= 'true')
    #else
        #set ($showDocs= 'false')
    #end
    
    #if ($advanced_options.getChild("sortReverse").value != "")
        #set ($sortReverse= 'true')
    #else
        #set ($sortReverse= 'false')
    #end
    
    #set ($sortPredicate= $advanced_options.getChild("sortPredicate").value)
    
    #set ( $uid = $advanced_options.getChild('uid').value )
    
    ## Display the HTML output of the plugin
    #set ($requireJSmodules= "'cso'")
    #set ($requireJScode= '')
    #DisplayRequireJSBlock($requireJSmodules $requireJScode)
    #DisplayCSOHTML($uid $showDocs $sortPredicate $sortReverse $validMajorList)


#end


#*
    DisplayCSOHTML
    
    Outputs the HTML of the CSO plugin, given the required settings
    
    @param $uid the unique id for this widget
    @param $showDocs when set to true, this will output the documentation
    @param $sortPredicate the name of the column to sort the data by
    @param $sortReverse when set to true, the data is sorted in reverse order
    @param $validMajorList comma seperated list of majors that will be included

*#
#macro (DisplayCSOHTML $uid $showDocs $sortPredicate $sortReverse $validMajorList)

<!--[if lte IE 9]>
<div role="alert" class="alert alert-danger">
    <p><strong>Your web browser is not capable of displaying this plugin.</strong></p>
	<p>If using Internet Explorer, version 9 or greater is required. If you are on a newer version of IE, please check that you are not in compatibility mode.</p>
</div>
<![endif]-->

<div ng-app="saitCsoWidgetApp" ng-init="widgetInstanceSettings = {uid: '${uid}', showDocs: ${showDocs}, sortPredicate: '${sortPredicate}', sortReverse: ${sortReverse}, initialFilter: { job_Major_list: [${validMajorList}]}}">

<div ng-view=""></div>

        <script type="text/ng-template" id="main.html">

            <div ng-show="feed.errorMessage" role="alert" class="alert alert-danger">
                {{feed.errorMessage}}
            </div>

            <div ng-show="feed.isLoading" role="status" style="text-align: center;">
                Loading job list...
            </div>

            <p ng-show="feed.jobs" role="status" aria-live="polite"><em>Currently showing {{(feed.jobs |
                filter:{
                job_status: 'active',
                job_JobTitle: userTitleFilter,
                JobLocation: userLocationFilter,
                job_positiontype_list: userTypeFilter
                }).length}} of {{feed.jobs.length}} jobs.</em></p>


            <div id="{{widgetInstanceSettings.uid}}" role="widget" ng-show="feed.jobs">

                <table ng-show="feed.jobs">
                    <tr>
                        <th scope="col"><label for="{{widgetInstanceSettings.uid}}_jobfilter">Title</label></th>
                        <th scope="col"><label for="{{widgetInstanceSettings.uid}}_locationfilter">Location</label></th>
                        <th scope="col"><label for="{{widgetInstanceSettings.uid}}_typefilter">Type</label></th>
                        <th scope="col">Closing Date</th>
                    </tr>
                    <tr>
                        <td>
                            <input aria-controls="{{widgetInstanceSettings.uid}}_tr" ng-model="userTitleFilter" id="{{widgetInstanceSettings.uid}}_jobfilter"/>
                        </td>
                        <td>
                            <input aria-controls="{{widgetInstanceSettings.uid}}_tr" ng-model="userLocationFilter" id="{{widgetInstanceSettings.uid}}_locationfilter"/>
                        </td>
                        <td>
                            <select aria-controls="{{widgetInstanceSettings.uid}}_tr" ng-model="userTypeFilter" id="{{widgetInstanceSettings.uid}}_typefilter">
                                <option value="">Show all</option>
                                <option ng-repeat="type in feed.positionTypes | orderBy:type" value="{{type}}">Includes {{type}}</option>

                            </select>
                        </td>
                        <td>
                            <a href="" ng-click="widgetInstanceSettings.sortReverse=!widgetInstanceSettings.sortReverse" ng-switch="widgetInstanceSettings.sortReverse">
                                Expires first <span ng-switch-when="false">on top</span><span ng-switch-when="true">on bottom</span>
                            </a>
                        </td>
                    </tr>
                    <tr ng-repeat="job in feed.jobs |
                            filter:{
                                job_status: 'active',
                                job_JobTitle: userTitleFilter,
                                JobLocation: userLocationFilter,
                                job_positiontype_list: userTypeFilter
                            } |
                            orderBy:widgetInstanceSettings.sortPredicate:widgetInstanceSettings.sortReverse"
                        aria-live="polite"
                        aria-relevant="additions removals"
                        id="{{widgetInstanceSettings.uid}}_tr">

                        <th scope="row"><a href="{{job.ExternalLink}}" target="_blank">{{job.job_JobTitle}}</a></th>
                        <td>{{job.JobLocation}}</td>
                        <td>{{job.job_positiontype_list}}</td>
                        <td>{{job.job_expiredate | date:'shortDate'}}</td>
                    </tr>
                </table>

                <p>Job list provided by <a href="http://umt.edu/career/">Career Services</a>.</p>

            </div>

        </script>



</div>



#end

#*
                ------------------
                --Utility Macros--
                ------------------
*#

#*
    NodesToCommaSeperatedString
    
    Outputs a comma seperated list (JavaScript compatible) from nodes.
    Example: Foo,Bar,Baz
    
    @param $nodes the nodes to output

*#
#macro (NodesToCommaSeperatedString $nodes)
#foreach ($node in $nodes)
#set ($value = $_SerializerTool.serialize($node, true))        
#if($velocityCount != $nodes.size())'${value}',#else'${value}'#end
#end
#end

#*
    DisplayRequireJSBlock
    
    Outputs a RequireJS code block for loading an angular module
    
    @param $modules the list of modules to load (surrounded by single quotes)
    @param $code JS code to be put inside the block

*#
#macro (DisplayRequireJSBlock $modules $code)
<script type="text/javascript">
    require([${modules}],function(){
        ${code}
    });
</script>
#end

## Format URL: http://cascade.umt.edu/entity/open.act?id=f994a3ff0a0a07cc2c11008e1ec0aa74&type=format&#highlight

## Audits
### 08-21-2014 17:46	db111722e	move
