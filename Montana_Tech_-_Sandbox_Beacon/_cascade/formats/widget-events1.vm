#######################################################################################################################################
## Format:          /_cascade/formats/widgets - events
## Project:         Montana Tech Events Listing with Filters
## Purpose:         Events Listing Widget
##
## History:         Developer       Description
## 12/12/2017       mj              Initial Version
#######################################################################################################################################

## Get the current page node and data node from the XML document
#set ( $page = $_XPathTool.selectSingleNode($contentRoot, "//system-page[@current='true']") )
#set ( $data = $_XPathTool.selectSingleNode($page, "system-data-structure") )

## Get the list of events (that have a date)
#set ( $events = $_XPathTool.selectNodes($contentRoot, "//system-page[system-data-structure/starts != '']") )

##Sort the events by date 
$_SortTool.addSortCriterion("system-data-structure/starts", "", "number", "ascending", "")
$_SortTool.sort($events)

## Get the category selections for the widget.
#set ( $widgetCategorySelections = $_XPathTool.selectNodes($contentRoot, "//system-page[@current='true']/system-data-structure/widget/latest-events/categories/value") )

#set ( $currentDate = $_DateTool.getDate() )
#set ( $eventCounter = 0 )


<section aria-label="Upcoming Events" class="upcoming--events">
    <h4>Upcoming Events</h4>
    <div class="container">

        #foreach ( $event in $events )
        
            ## Get the categories for the current event
            #set ( $eventCategorySelections = "" )
            #set ( $eventCategorySelections = $_XPathTool.selectNodes($event, "dynamic-metadata[name='categories']/value") )
            
            ## Check to see if we find a match between the widget category selection(s) and the event selection(s)
            #set ( $categoryMatch = false )
            #foreach ( $widgetCategorySelection in $widgetCategorySelections )
                #foreach ( $eventCategorySelection in $eventCategorySelections )
                    #if ( $widgetCategorySelection.value == $eventCategorySelection.value )
                        #set ( $categoryMatch = true )
                    #end
                #end
            #end
            
            #if ( $categoryMatch )
            
                ## Initialize variables
                #set ( $title = "" )
                #set ( $eventLink = "" )
                #set ( $eventStartDate = "" )
                
                ## Set variables
                #set ( $title = $_EscapeTool.xml($event.getChild("title").value) )
                #set ( $eventLink = $event.getChild("path").value )
                #set ( $eventStartDate = $_DateTool.getDate($_XPathTool.selectSingleNode($event, "system-data-structure/starts").value ))
                
                ## If the event date is today or later, continue...
                #if ( $_DateTool.difference($currentDate, $eventStartDate).getDays() >= 0 )
                
                    #set ( $eventCounter = $eventCounter + 1 )
                    
                    ## Only write out 6 Events
                    #if ( $eventCounter > 6 )
                        #break
                    #end
                         
                    <article>
                        <a title="${title}" href="${eventLink}">
                            <div class="colText">
                                <div class="wrap">
                                    <div class="date">$_DateTool.format("MMMM dd, yyy", $eventStartDate)</div>
                                    <h3 class="content">${title}</h3>
                                </div>
                            </div>
                        </a>
                    </article>
                #end
            #end
        #end
        <a href="[system-asset:id=d184aed70a0a2873443ef4c0b5e81aa1]/calendar/index[/system-asset]">Events Calendar</a>
    </div>
</section>

## Format URL: http://cascade.umt.edu/entity/open.act?id=5543b2c00a0a287328ca3f9337236516&type=format&#highlight

## Audits
### 12-14-2017 13:43	mj129236e	create
### 02-19-2018 14:43	mj129236e	move
