#set ($pageStructure = $_XPathTool.selectSingleNode($contentRoot, "/system-index-block"))
#set ($profile = $_XPathTool.selectSingleNode($pageStructure, "calling-page/system-page"))
#set ($path = $_SerializerTool.serialize($profile.getChild("path"), true))
#set ($profile = $_XPathTool.selectSingleNode($contentRoot, "calling-page/system-page/system-data-structure") )
#set ($courseList = $_XPathTool.selectNodes($pageStructure, "system-page"))
#set ($courses = [])
#set ($content = "")
#set ($firstName = $_SerializerTool.serialize($profile.getChild("required").getChild("first-name"), true))
#set ($lastName = $_SerializerTool.serialize($profile.getChild("required").getChild("last-name"), true))
#set ($type = $_SerializerTool.serialize($profile.getChild("required").getChild("type"), true))
#set ($photo = $_SerializerTool.serialize($profile.getChild("image").getChild("profile-image").getChild("path"), true))
#set ($titles = $_XPathTool.selectNodes($profile, "contact/title"))
#set ($phone = $_SerializerTool.serialize($profile.getChild("contact").getChild("phone"), true))
#set ($email = $_SerializerTool.serialize($profile.getChild("contact").getChild("email"), true))
#set ($office = $_SerializerTool.serialize($profile.getChild("contact").getChild("office"), true))
#set ($content = $_SerializerTool.serialize($profile.getChild("content"), true))

##Active?
#set ($active = $profile.getChild("required").getChild("active").value)

##Check if there's no photo, and if there isn't, make it the silhouette.
#if ($photo == "/")
    #set ($photo = "/imx/placeholder.png")
#end

##Hours
#set ($monday = $_SerializerTool.serialize($profile.getChild("contact").getChild("officehours").getChild("monday"), true))
#set ($tuesday = $_SerializerTool.serialize($profile.getChild("contact").getChild("officehours").getChild("tuesday"), true))
#set ($wednesday = $_SerializerTool.serialize($profile.getChild("contact").getChild("officehours").getChild("wednesday"), true))
#set ($thursday = $_SerializerTool.serialize($profile.getChild("contact").getChild("officehours").getChild("thursday"), true))
#set ($friday = $_SerializerTool.serialize($profile.getChild("contact").getChild("officehours").getChild("friday"), true))

##Titles
#set ($titleString = "")
#foreach ($title in $titles)
    #set ($titleString = "$title.value/$titleString")
#end
#set ($titleString = "$titleString.")
#set ($titleString = $_StringTool.substringBefore($titleString, "/."))

## Populate "courses" array with courses that instructor teaches.
#foreach ($course in $courseList)
    #set ($instructors = $_XPathTool.selectNodes($course.getChild("system-data-structure").getChild("instructors"), "instructor"))
    #foreach ($instructor in $instructors)
        #set ($coursePath = $_SerializerTool.serialize($instructor.getChild("path"), true))
        #if ($coursePath == $path)
            #set ($dummy = $courses.add($course))
        #end
    #end
#end

<h1 class="maroon">$firstName $lastName</h1>
<h3>$titleString</h3>
<img alt="$firstName $lastName" class="profilepic" src="$photo"/>
<table class="profiletable" summary="$firstName $lastName Contact Information">
    <tr>
        <td class="cola"><strong>Phone:</strong></td>
        <td class="colb">
            #if($phone != "<phone />")
                $phone
            #else
                No phone provided.
            #end
        </td>
    </tr>
    <tr>
        <td class="cola"><strong>Email:</strong></td>
        <td class="colb">
            #if($email != "<email />")
                <a href="mailto:$email" alt="Email $firstName $lastName">$email</a>
            #else
                No email provided.
            #end
        </td>
    </tr>
    <tr>
        <td class="cola"><strong>Office:</strong></td>
        <td class="colb">
            #if($office != "<office />")
                $office
            #else
                No office provided.
            #end
        </td>
    </tr>
    <tr>
        <td class="cola hours"><strong>Office Hours:</strong></td>
        <td class="colb hours">
            #if ($monday == "<monday />" && $tuesday == "<tuesday />" && $wednesday == "<wednesday />" && $thursday == "<thursday />" && $friday == "<friday />")
                No office hours provided.
            #else
                <table class="officehours">
                    <tr>
                        <td class="day">Monday:</td>
                        <td>
                            #if ($monday != "<monday />")
                                $monday
                            #else
                                None.
                            #end
                        </td>
                    </tr>
                    <tr>
                        <td class="day">Tuesday:</td>
                        <td>
                            #if ($tuesday != "<tuesday />")
                                $tuesday
                            #else
                                None.
                            #end
                        </td>
                    </tr>
                    <tr>
                        <td class="day">Wednesday:</td>
                        <td>
                            #if ($wednesday != "<wednesday />")
                                $wednesday
                            #else
                                None.
                            #end
                        </td>
                    </tr>
                    <tr>
                        <td class="day">Thursday:</td>
                        <td>
                            #if ($thursday != "<thursday />")
                                $thursday
                            #else
                                None.
                            #end
                        </td>
                    </tr>
                    <tr>
                        <td class="day">Friday:</td>
                        <td>
                            #if ($friday != "<friday />")
                                $friday
                            #else
                                None.
                            #end
                        </td>
                    </tr>
                </table>
            #end
        </td>
    </tr>
    <tr>
        <td class="cola"><strong>Courses:</strong></td>
        <td class="colb">
            <div class="coursecontainer">
                #if ($courses == [])
                    This faculty member is not assigned to teach any courses within the department.
                #end
                #foreach ($course in $courses)
                    #set ($coursename = $_SerializerTool.serialize($course.getChild("system-data-structure").getChild("course").getChild("title"), true))
                    #set ($coursenumber = $_SerializerTool.serialize($course.getChild("system-data-structure").getChild("course").getChild("number"), true))
                    #set ($link = $course.getChild("path").value)
                    #if ($coursenumber != "")
                        <a href="$link">$coursenumber - $coursename</a><br/>
                    #end
                #end
            </div>
        </td>
    </tr>
</table>
$content
#if ($active != "Yes")
    <br/><strong>This faculty member is currently not active.</strong>
#end

## Format URL: http://cascade.umt.edu/entity/open.act?id=e0df34cc0a0a07cc1517225ff5713bdb&type=format&#highlight

## Audits
### 10-28-2013 22:54	pe205482	edit
### 10-28-2013 22:53	pe205482	startedit
### 10-28-2013 22:54	pe205482	startedit
### 10-28-2013 22:55	pe205482	edit
### 10-28-2013 22:55	pe205482	edit
### 10-28-2013 22:55	pe205482	startedit
### 10-28-2013 22:55	pe205482	startedit
### 10-28-2013 22:56	pe205482	edit
### 10-28-2013 22:57	pe205482	edit
### 10-28-2013 22:56	pe205482	startedit
