#set ($pageStructure = $_XPathTool.selectSingleNode($contentRoot, "/system-data-structure") ) 

#set ($configurations = $_XPathTool.selectSingleNode($pageStructure, "configuration")) 
#set ($show_toc = $configurations.getChild("show-toc").getChild("value").value)
#set ($collapse_answers= $configurations.getChild("collapse-answers").getChild("value").value)


#set ($sections  = $_XPathTool.selectNodes($pageStructure, "section")) 
#set ( $number_of_sections = $sections.size() )

#if ($show_toc == 'Yes')
    <div class="faq-toc faq-border-rounded-bottom faq-border-rounded-top">
        #foreach ($section in $sections)
            #set ($section_title = $_SerializerTool.serialize($section.getChild("section-title"), true))
            #set ($section_id = $foreach.count)
            <ul class="faq-section">
                <li>
                    <a href="#section_${section_id}" class="faq-toc-section-link">
                        ${section_title}
                    </a>
                    <ul>
                    
                        #set ($faqs = $_XPathTool.selectNodes($section, "faq")) 
                        
                        #foreach ($faq in $faqs)
                            #set ($faq_id = $foreach.count)
                            #set ($question = $_SerializerTool.serialize($faq.getChild("question"), true))
                            <li><a href="#section_${section_id}_question_${faq_id}">${question}</a></li>
                        #end
                        
                    </ul>
                </li>
            </ul>
        #end
    </div>
#end


#foreach ($section in $sections)
    #set ($section_title = $_SerializerTool.serialize($section.getChild("section-title"), true))
    #set ($section_id = $foreach.count)
    <div class="faq-section">
        
        #if($number_of_sections > 1)
            <a name="section_${section_id}"/>
            <h2>${section_title}</h2>
        #end
        
        <dl>
        
            #set ($faqs = $_XPathTool.selectNodes($section, "faq")) 
            
            #foreach ($faq in $faqs)
                #set ($question = $_SerializerTool.serialize($faq.getChild("question"), true))
                #set ($answer = $_SerializerTool.serialize($faq.getChild("answer"), true))
                #set ($faq_id = $foreach.count)
                <dt class="faq-border-rounded-top">
                    #if ($collapse_answers == 'Yes')
                       <a href="#section_${section_id}_question_${faq_id}" class="faq-collapse-trigger" name="section_${section_id}_question_${faq_id}">
                           ${question} <div style="float:right;" class="icon"></div>
                       </a>
                    #else
                        <a href="#section_${section_id}_question_${faq_id}" class="faq-collapse-trigger" name="section_${section_id}_question_${faq_id}" />
                        ${question}
                    #end
                </dt>
                <dd class="faq-border-rounded-bottom">${answer}</dd>
            #end
            
        </dl>
    </div>
#end

<style>
    .faq-toc { border: 1px solid #e8e8e8; padding: 15px; margin: 0; background-color: #f7f7f7;  }
    
    .faq-toc h2{ margin-top: 0;  }
    
    .faq-toc ul.faq-section {margin: 0; padding: 0;}
    .faq-toc ul.faq-section ul {padding-left: 15px; margin: 0;}
    .faq-toc ul.faq-section li {margin: 0; padding: 0; list-style-type: none; list-style-image: none;}
    
    ul.faq-section a.faq-toc-section-link {font-weight: bold;}
    
    div.faq-section{ padding: 15px 0 5px 0; margin: 0 0 15px 0;  }
    
    div.faq-section dl{  }
    
    div.faq-section dl dt{ 
        font-weight: bold; 
        border: 1px solid #e8e8e8; 
        padding: 0px; 
        margin: 5px 0 0 0; 
        background-color: #f7f7f7;
     }
     
     div.faq-border-rounded-top dt{
         cursor: hand;
     }
    
    div.faq-section dl dd{ border: 1px solid #e8e8e8; border-top: 0; padding: 15px; margin: 0 0 5px 0; background-color: #FFF; color: #606060; }
    
    .faq-collapse-trigger {
        cursor: pointer;
        cursor: hand;
        display: block;
     }
     
    .faq-border-rounded-top a, .faq-border-rounded-top a:visited {
        text-decoration:none;
        padding: 15px;
    }
    .faq-border-rounded-top a:hover{
         background-color:   #EEE;  
     }
    
          
    .faq-collapse-trigger small { float: right; font-size: 10px; color: #ccc; }
    
    .faq-border-rounded-top {
        -webkit-border-top-left-radius: 3px;
        -webkit-border-top-right-radius: 3px;
        -moz-border-radius-topleft: 3px;
        -moz-border-radius-topright: 3px;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
        cursor: pointer;
        cursor: hand;
    }
    
    .faq-border-rounded-bottom {
        -webkit-border-bottom-right-radius: 3px;
        -webkit-border-bottom-left-radius: 3px;
        -moz-border-radius-bottomright: 3px;
        -moz-border-radius-bottomleft: 3px;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    
    
    .icon{
        width: 16px;
        height: 16px;
        background-image: url('http://life.umt.edu/_common/icons/bullet_arrow_up.png');
    }
    
    .bullet_down{
        background-image: url('http://life.umt.edu/_common/icons/bullet_arrow_down.png');
    }

</style>

#if ($collapse_answers == 'Yes')
    <script type="text/javascript">
    require(["jquery"],function(jQuery) {
        jQuery(document).ready(function() {
            
            jQuery("div.faq-section dd").hide();
            jQuery("div.faq-section dd").attr("aria-hidden", "true");
            
            jQuery("div.faq-section dt").click(function()
            {
                jQuery(this).next("div.faq-section dd").slideToggle(500);
                jQuery(this).children().children('.icon').toggleClass('bullet_down');                
                var aria_attrib = jQuery(this).next("div.faq-section dd").attr("aria-hidden");
                
                if(aria_attrib == "true"){
                    jQuery(this).next("div.faq-section dd").attr("aria-hidden", "false");
                }else{
                    jQuery(this).next("div.faq-section dd").attr("aria-hidden", "true");
                }
                
                return false;
                
            });
        });
    });
    </script>
#end

## Format URL: http://cascade.umt.edu/entity/open.act?id=0d3960570a0a07cc79dd8f74768b6909&type=format&#highlight

## Audits
