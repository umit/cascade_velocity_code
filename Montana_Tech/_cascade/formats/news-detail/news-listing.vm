#######################################################################################################################################
## Format:          /formats/news-listing
## Project:         Montana Tech
## Purpose:         Page - Displays all user defined content from the News Listing Page
##
## History:         Developer       Description
## 07/19/2017       mj              Initial
#######################################################################################################################################

## Get the current page's system-data-structure node from the XML document
#set ( $page = $_XPathTool.selectSingleNode($contentRoot, "//system-page[@current='true']") )
#set ( $data = $_XPathTool.selectSingleNode($contentRoot, "//system-page/system-data-structure") )

#######################################################################
## Right Column
#######################################################################
 
#set ( $displayLeftCol = "Yes" )
#set ( $leftColValue = "Yes" )
#set ( $right = "col9 right content" )
 
<div class="${right}">

#######################################################################
## Articles
#######################################################################
 
## Get the selected year
#set ( $listingPageYear = "" )
#set ( $listingPageYear = $_XPathTool.selectSingleNode($page, "dynamic-metadata[name='year']/value").value )
    
## Get the list of articles (that have a release date)
## #set ( $newsArticles = $_XPathTool.selectNodes($contentRoot, "//system-page/system-data-structure/page-content/news-article") )
#set ( $newsArticles = $_XPathTool.selectNodes($contentRoot, "//system-page[system-data-structure/start_date !='']") )
   
##Sort the articles by date descending
$_SortTool.addSortCriterion("system-data-structure/start_date", "", "number", "descending", "")
$_SortTool.sort($newsArticles)

<div class="full-wrapper wysiwyg news">
<div class="wrapper row">

## Initialize Variables
#set ( $counter = 0 )
#set ( $prevMonth = "" )

## Loop through the articles to write them out.
#foreach ( $newsArticle in $newsArticles )
       
## Initialize variables
#set ( $headline = "" )
#set ( $displayName = "" )
#set ( $summary = "" )
#set ( $newsLink = "" )
#set ( $categoryString = "" )
#set ( $newsDate = "" )
#set ( $newsDateFormatted = "" )
#set ( $newsMonth = "" )
#set ( $newsMonthAndYear = "" )
#set ( $newsYear = "" )
           
## Set variables
##set ( $headline = $_XPathTool.selectSingleNode($newsArticle, "system-data-structure/page-content/news-article/headline").value )
##set ( $headline = $_EscapeTool.xml($newsArticle.getChild("headline").value) )
#set ( $displayName = $_EscapeTool.xml($newsArticle.getChild("display-name").value) )
#set ( $summary = $_XPathTool.selectSingleNode($newsArticle, "system-data-structure/summary").value )

## #set ( $summary = $_EscapeTool.xml($newsArticle.getChild("summary").value) )
#set ( $newsLink = $newsArticle.getChild("link").value )
## #set ( $newsLink = $contentRoot.getChild("system-page").getChild("path").value )
#set ( $newsCategories = $_XPathTool.selectNodes($newsArticle, "system-data-structure/categories/value") )
#set ( $newsDate = $_DateTool.getDate($_XPathTool.selectSingleNode($newsArticle, "system-data-structure/start_date").value ))
         
#set ( $newsDateFormatted = $_DateTool.format('MM/dd/yyyy', $newsDate) )
#set ( $newsMonth = $_DateTool.format('MMMM', $newsDate) )
#set ( $newsMonthAndYear = $_DateTool.format('MMMM yyyy', $newsDate) )
#set ( $newsYear = $_DateTool.format('yyyy', $newsDate) )
           
#set ( $counter = $counter + 1 )
              
##Build the category string whichi is used in the HTML for filtering.
#foreach ( $newsCategory in $newsCategories )
#set ( $categoryString = $categoryString + " " + $newsCategory.value.replaceAll(" ", "_").replaceAll(",", "") )
#end
#set ( $categoryString = $categoryString.trim() )
 
## HTML is broken out by month.  Check to see if we're on a new one.
#if ( $newsMonth != $prevMonth )               
#set ( $prevMonth = $newsMonth )               
               
<!-- start month section -->
                  
## Show the year for the first article
#if ( $counter == 1 )
## <h2 date="${listingPageYear}">${listingPageYear}</h2>
#end
## <h2 class="newsMonth" date="${newsMonthAndYear}">${newsMonth}</h2>
#end

#set ( $articleDate = $_DateTool.getDate($_XPathTool.selectSingleNode($contentRoot, "//system-page/system-data-structure/start_date").value) )
#set ( $formattedDate = $_DateTool.format('MM/dd/yyyy', $_DateTool.toDate('MM-dd-yyyy', $articleDate)) )
               
## Show the news info.

#if ( $displayName != "" )
<article>
<h3><a href="${newsLink}" title="${displayName}">${displayName}</a></h3>
<div class="articleListing ${categoryString}" date="${newsMonthAndYear}">
<p class="date">${newsDateFormatted}</p>
<p class="summary">$_EscapeTool.xml($summary)
... <a href="${newsLink}" title="$_EscapeTool.xml(displayName)">Read More</a></p>
</div>
</article>
             
#end
#end
</div>  
</div>  

</div> ## end right column
<![CDATA[#protect</div>#protect]]> ## end full-wrapper interior-content  

## Format URL: http://cascade.umt.edu/entity/open.act?id=564a54000a0a07cc545fdb45fee8edb8&type=format&#highlight

## Audits
### 12-06-2017 14:47	mj129236e	startedit
### 12-06-2017 14:51	mj129236e	edit
### 12-06-2017 14:52	mj129236e	startedit
### 12-06-2017 14:57	mj129236e	edit
### 12-06-2017 15:06	mj129236e	startedit
### 12-06-2017 15:28	mj129236e	edit
### 07-18-2017 15:21	mj129236e	create
### 07-18-2017 15:23	mj129236e	startedit
### 07-18-2017 15:24	mj129236e	edit
### 07-18-2017 15:26	mj129236e	startedit
