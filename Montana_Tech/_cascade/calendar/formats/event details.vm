#*
     Turns a non-negative number into an ordinal string used to denote the position in an 
     ordered sequence, such as 1st, 2nd, 3rd, 4th.
*#
#macro ( ordinalize $number)
    #set ($number = $_NumberTool.integer($number))
    #set ($numberStr = $number.toString())

    #if (11 <= $number && $number <= 13) 
        ${numberStr}th 
    #else
        #set ($remainder = $_MathTool.mod($number, 10))
        #if ($remainder == 1) ${numberStr}st
        #elseif ($remainder == 2) ${numberStr}nd
        #elseif ($remainder == 3) ${numberStr}rd
        #else ${numberStr}th
        #end
    #end    
#end

#set ($page         = $_XPathTool.selectSingleNode($contentRoot, "/system-index-block/calling-page/system-page"))
#set ($data         = $_XPathTool.selectSingleNode($page, "system-data-structure"))
#set ($start        = $_DateTool.getDate($data.getChild("starts").value))
#set ($end          = $_DateTool.getDate($data.getChild("ends").value))
#set ($startDate    = $_DateTool.format("MMMM dd, yyyy", $start))
#set ($endDate      = $_DateTool.format("MMMM dd, yyyy", $end))
#set ($recurrence   = $data.getChild("recurrence"))
#set ($categories   = $_XPathTool.selectNodes($page, "dynamic-metadata[name='categories']/value"))
#set ($colorMapping = $_XPathTool.selectSingleNode($contentRoot, "//system-data-structure[category/csscolor]"))
#set ($details      = $data.getChild("details"))
#set ($location     = $data.getChild("location"))
#set ($contact      = $data.getChild("contact"))
#set ($contactName  = $contact.getChild("name"))
#set ($contactSite  = $contact.getChild("website"))
#set ($contactPhone = $contact.getChild("phone"))
#set ($contactEmail = $contact.getChild("email"))
#set ($relatedLinks = $_XPathTool.selectNodes($data, "relatedlink[title!='']"))
#set ($allDayEvent  = $data.getChild("all-day").getChild("value").value )

#set ( $ImageGroup = $data.getChild("image") )
#set ( $Image = $ImageGroup.getChild("image") )
#set ( $ImagePath = $Image.getChild("path") )
#set ( $ImageAltText = $ImageGroup.getChild("alt_text") )
#set ( $PageName = $_XPathTool.selectSingleNode($page, "name") )

#set ( $iCalLink = $page.getChild("link") )


<section class="interior-content">

    <div class="wrapper row wysiwyg">
        <div class="wysiwyg"><p><label class="eventDetail"><strong>Date:</strong></label>
    
        <span>#if ($startDate == $endDate)
            ${startDate}
        #elseif ($startDate != $endDate)
            ${startDate} - ${endDate}
        #end
        </span></p>
 <br/>
 
    #if ( $allDayEvent == "Yes" )
        <label class="eventDetail"><strong>All Day</strong></label>
    #else
        <label class="eventDetail"><strong>Times:</strong></label>
        $_DateTool.format("hh:mm a", $start)#if ($_DateTool.format("hh:mm a", $end) != "01:00 AM") - $_DateTool.format("hh:mm a", $end)#end
    #end
    
    
   
    <br/>
    <label class="eventDetail"><strong>Location:</strong></label>
   
    <span>
        #if ($location.value != "")
            $_EscapeTool.xml($location.value)
        #else
            N/A
        #end
    </span>
    
    
    
    
    #if ($contactName.value != "" || $contactEmail.value != "" || $contactPhone.value != "" || $contactSite.value != "")
        #if($contactName.value != "")
            <br/><br/>
            <label class="eventDetail"><strong>Contact Name:</strong></label>
            <span>$_EscapeTool.xml($contactName.value)</span>
        #end
        
        #if($contactSite.value != "")
        
            #if (!$contactSite.value.toLowerCase().contains("http://"))  
                #set ( $contactSite = "http://" + $contactSite.value )
            #else
                #set ( $contactSite = $contactSite.value )
            #end  
        
            <br/>
            <label class="eventDetail"><strong>Website:</strong></label>
            <span><a href="$_EscapeTool.xml($contactSite)" target="_blank">$_EscapeTool.xml($contactSite)</a></span>
        #end
            
        #if($contactPhone.value != "")
            <br/><br/>
            <label class="eventDetail"><strong>Phone:</strong></label>
            <span>$_EscapeTool.xml($contactPhone.value)</span>
        #end
        
        #if($contactEmail.value != "")
            <br/><br/>
            <label class="eventDetail"><strong>Email:</strong></label>
            <span><a href="mailto:${_EscapeTool.url($contactEmail.value)}">${_EscapeTool.xml($contactEmail.value)}</a></span>
        #end
    #end
    
    
    
    
#if ($details.value != "")

    <br/>
    <label class="eventDetail"><strong>Details:</strong></label><br/>
    $_SerializerTool.serialize($details, true)

  
#end
</div>
#if ($relatedLinks.size() > 0)

    <div class="wysiwyg">
    <label class="eventDetail"><strong>Link(s):</strong></label>
    
        #foreach ($link in $relatedLinks)
            #set ($title = $link.getChild("title").value)

            #set ($internalLink = $_XPathTool.selectSingleNode($link, "internal/path").value)
            #set ($managedLink = $_XPathTool.selectSingleNode($link, "external/path").value)
            #set ($fileLink = $_XPathTool.selectSingleNode($link, "link-file/path").value)
            #set ($customLink = $_XPathTool.selectSingleNode($link, "custom").value)

            ## Determine which link field to use for the related link.
            #set ($morelink = "")
            #set ($morelinkexternal = "")
            #if ($internalLink != "/")
                #set ($morelink = $internalLink)
            #elseif ($fileLink != "/")
                #set ($morelinkexternal = $fileLink)
            #elseif ($customLink != "" && $customLink != "http://"  && $customLink != "https://")
                #set ($morelinkexternal = $_EscapeTool.xml($customLink))
            #elseif ($managedLink != "/")
                #set ($morelinkexternal = $managedLink)
            #end
            <ul>
                #if ($morelink != "")
                    <li><a href="${morelink}">${_EscapeTool.xml($title)}</a></li>
                #end

                #if ($morelinkexternal != "")
                    <li><a href="${morelinkexternal}" target="_blank">${_EscapeTool.xml($title)}</a></li>
                #end
            </ul>
        #end
    </div>
#end

<div class="col6">
<a class="btn btn-green" style="float:none;" href="[system-asset:configuration=iCal]${iCalLink.value}[/system-asset:configuration]">Download as iCal</a>
        #if ( $ImagePath.value != "/" )
            #if ( $ImageAltText.value != "" )
                <img class="eventDtlImg" src="${ImagePath.value}" alt="${ImageAltText.value}" />
            #else
                <img class="eventDtlImg" src="${ImagePath.value}" alt="${PageName.value}" />
            #end
        #end
</div>
<div class="col6" style="text-align:right;">
    <a class="btn btn-green" style="float:none;" href="/calendar/index">Return to Event Calendar</a>
</div>
    </div>
  
       
</section>



## Format URL: http://cascade.umt.edu/entity/open.act?id=7f059aa90a0a07cc545fdb45eb2abbe2&type=format&#highlight

## Audits
### 01-29-2018 17:45	mj129236e	startedit
### 01-29-2018 17:45	mj129236e	edit
### 01-29-2018 17:45	mj129236e	startedit
### 01-29-2018 17:46	mj129236e	edit
### 01-29-2018 17:46	mj129236e	activate_version
### 12-14-2017 14:34	mj129236e	startedit
### 12-14-2017 14:35	mj129236e	edit
### 12-14-2017 14:36	mj129236e	startedit
### 12-14-2017 14:36	mj129236e	edit
### 07-26-2017 13:11	mh129234e	create
